//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

/**
 * TokenType describes the valid types of syntax tokens.
 */
export enum TokenType {
    DurationLiteral = "DurationLiteral",
    ScalarLiteral = "ScalarLiteral",
    Identifier = "Identifier",
    BinaryAdd = "BinaryAdd",
    BinarySub = "BinarySub",
    BinaryTimes = "BinaryTimes",
    ParenLeft = "ParenLeft",
    ParenRight = "ParenRight",
    Comma = "Comma",
}

/**
 * Location represents a source location, i.e. a substring, of a given expression.
 */
export class Location {
    constructor(public readonly offset: number, public readonly length: number = -1) { }

    toString(): string {
        return `${this.offset}:${this.length}`
    }
}

/**
 * Token represents a single syntax token.
 */
export class Token {
    constructor(public readonly location: Location, public readonly type: TokenType, public readonly value: string) { }
}