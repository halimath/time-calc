//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Scanner, SyntaxError } from "../scanner"
import { Location, TokenType, Token } from "../token"
import { AstNode, BinaryOperator, BinaryOperatorNode, DurationNode, ScalarNode, ReferenceNode } from "../ast"
import { DurationParser, ComponentDurationParser } from "../duration"
import { FunctionCall } from "../ast/node";

/**
 * `ParseError` represents errors that occur during parsing.
 */
export class ParseError {
    static fromSyntaxError(e: SyntaxError): ParseError {
        return new ParseError(e.message, e.location)
    }

    constructor(public message: string, public location: Location) { }

    toString(): string {
        return `${this.message} @ ${this.location}`
    }
}

/**
 * `Parser` implements parsing of time-calc expression. The input is
 * drawn from a `Scanner`. The output is an abstract syntax tree (AST)
 * returned as the AST's root `AstNode`.
 */
export class Parser {
    constructor(private scanner: Scanner, private durationParser: DurationParser = new ComponentDurationParser()) { }

    expression(): AstNode {
        try {
            return this.addSubExpression()
        } catch (e) {
            if (e instanceof ParseError) {
                throw e
            }

            if (e instanceof SyntaxError) {
                throw ParseError.fromSyntaxError(e)
            }

            throw new ParseError(`unexpected error: ${e}`, new Location(0))
        }
    }

    private addSubExpression(): AstNode {
        let op1 = this.mulExpression()

        while (true) {
            const t = this.scanner.next()
            if (t === null) {
                return op1
            }

            let operator: BinaryOperator
            switch (t.type) {
                case TokenType.ParenRight:
                case TokenType.Comma:
                    this.scanner.pushBack(t)
                    return op1
                case TokenType.BinaryAdd:
                    operator = BinaryOperator.Add
                    break
                case TokenType.BinarySub:
                    operator = BinaryOperator.Sub
                    break
                default:
                    throw new ParseError(`expected + or - but got: "${t.value}" (${t.type})`, t.location)
            }

            const op2 = this.mulExpression()
            if (op2 === null) {
                throw new ParseError(`expected expression but got EOE`, t.location)
            }
            op1 = new BinaryOperatorNode(t.location, operator, op1, op2)
        }
    }

    private mulExpression(): AstNode {
        let op1 = this.atom()

        while (true) {
            const t = this.scanner.next()
            if (t === null) {
                return op1
            }

            let operator: BinaryOperator
            switch (t.type) {
                case TokenType.BinaryAdd:
                case TokenType.BinarySub:
                case TokenType.ParenRight:
                case TokenType.Comma:
                    this.scanner.pushBack(t)
                    return op1
                case TokenType.BinaryTimes:
                    operator = BinaryOperator.Times
                    break
                default:
                    throw new ParseError(`expected * but got: "${t.value} (${t.type})"`, t.location)
            }

            const op2 = this.atom()
            if (op2 === null) {
                throw new ParseError(`expected expression but got EOE`, t.location)
            }
            op1 = new BinaryOperatorNode(t.location, operator, op1, op2)
        }
    }

    private atom(): AstNode {
        const t = this.scanner.next()

        if (t === null) {
            throw new ParseError(`expected duration, literal, reference, function call or ( but got EOE`, new Location(0))
        }

        switch (t.type) {
            case TokenType.DurationLiteral:
                try {
                    const d = this.durationParser.parse(t.value)
                    return new DurationNode(t.location, d)
                } catch (e) {
                    throw new ParseError(`invalid duration "${t.value}": ${e}`, t.location)
                }
            case TokenType.ScalarLiteral:
                const value = parseFloat(t.value)
                if (isNaN(value)) {
                    throw new ParseError(`not a valid scalar: "${t.value}"`, t.location)
                }
                return new ScalarNode(t.location, value)
            case TokenType.Identifier:
                const n = this.scanner.next()
                if (n === null || n.type !== TokenType.ParenLeft) {
                    if (n !== null) {
                        this.scanner.pushBack(n)
                    }
                    return new ReferenceNode(t.location, t.value)
                }
                return this.functionCall(t)
            case TokenType.ParenLeft:
                const e = this.expression()
                let pr = this.scanner.next()
                if (pr === null) {
                    throw new ParseError(`expected ) but got EOE`, e.location)
                }
                if (pr.type !== TokenType.ParenRight) {
                    throw new ParseError(`expected ) but got ${pr}`, pr.location)
                }
                return e
            default:
                throw new ParseError(`expected duration, literal or reference but got "${t.value}" (${t.type})`, t.location)
        }
    }

    private functionCall(identifier: Token): AstNode {
        const args: Array<AstNode> = []

        while (true) {
            const arg = this.expression()
            if (arg !== null) {
                args.push(arg)
            }

            const t = this.scanner.next()
            if (t === null) {
                throw new ParseError(`expected , or ) but got EOE`, identifier.location)
            }

            if (t.type === TokenType.Comma) {
                continue
            }

            if (t.type === TokenType.ParenRight) {
                break
            }

            throw new ParseError(`expected expression or ) but got ${t}`, t.location)
        }

        return new FunctionCall(identifier.location, identifier.value, args)
    }
}