//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Location } from "../token"
import { Duration } from "../duration";

export interface AstNode {
    readonly location: Location
    accept(visitor: AstNodeVisitor): void
}

export class DurationNode implements AstNode {
    constructor(public readonly location: Location, public readonly duration: Duration) { }

    accept(visitor: AstNodeVisitor): void {
        visitor.visitDuration(this)
    }
}

export class ScalarNode implements AstNode {
    constructor(public readonly location: Location, public readonly value: number) { }

    accept(visitor: AstNodeVisitor): void {
        visitor.visitScalar(this)
    }
}

export class ReferenceNode implements AstNode {
    constructor(public readonly location: Location, public readonly name: string) { }

    accept(visitor: AstNodeVisitor): void {
        visitor.visitReference(this)
    }

}

export enum BinaryOperator {
    Add = "Add",
    Sub = "Sub",
    Times = "Times",
}

export class BinaryOperatorNode implements AstNode {
    constructor(public readonly location: Location, public readonly operator: BinaryOperator, public readonly leftOperand: AstNode, public readonly rightOperand: AstNode) { }

    accept(visitor: AstNodeVisitor): void {
        visitor.visitBinaryOperator(this)
    }
}

export class FunctionCall implements AstNode {
    constructor(public readonly location: Location, public readonly functionName: string, public readonly args: Array<AstNode>) { }

    accept(visitor: AstNodeVisitor): void {
        visitor.visitFunctionCall(this)
    }
}

export interface AstNodeVisitor {
    visitDuration(node: DurationNode): void
    visitScalar(node: ScalarNode): void
    visitReference(node: ReferenceNode): void
    visitBinaryOperator(node: BinaryOperatorNode): void
    visitFunctionCall(node: FunctionCall): void
}
