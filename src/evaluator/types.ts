//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration } from "../duration"

/**
 * `EvaluationError` represents an error during evaluation.
 */
export class EvaluationError {
    constructor(public readonly message: string) { }

    toString(): string {
        return this.message
    }
}

/**
 * `EvaluationResult` defines the result type of an expression
 * evaluation which can either be a `Duration` or a `number`.
 */
export type EvaluationResult = Duration | number

/**
 * `isDuration` is a type guard to test whether the given value is a `Duration`.
 * @param v the value
 */
export function isDuration(v: EvaluationResult): v is Duration {
    return v instanceof Duration
}

/**
 * `isScalar` is a type guard to test whether the given value is a `number`.
 * @param v the value
 */
export function isScalar(v: EvaluationResult): v is number {
    return typeof v === "number"
}

/**
 * `EvaluationFunction` defines the type for functions that can be called
 * during evaluation.
 */
export interface EvaluationFunction {
    (args: Array<EvaluationResult>): EvaluationResult
}

/**
 * `EnvironmentValue` defines the valid types for values in a `Environment`.
 */
export type EnvironmentValue = EvaluationResult | EvaluationFunction

/**
 * `isEvaluationFunction` is a type guard to test whether the given value is a `EvaluationFunction`.
 * @param v the value
 */
export function isEvaluationFunction(v: EnvironmentValue): v is EvaluationFunction {
    return typeof v === "function"
}

/**
 * `isEvaluationResult` implements a type guard that tests whether the given value is an `EvaluationResult`.
 * @param v the value
 */
export function isEvaluationResult(v: EnvironmentValue): v is EvaluationResult {
    return !isEvaluationFunction(v)
}