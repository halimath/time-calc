//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { DurationParser, Duration, ComponentDurationParser } from "../duration"
import { AstNodeVisitor, DurationNode, ScalarNode, ReferenceNode, BinaryOperatorNode, BinaryOperator } from "../ast"
import { Parser } from "../parser"
import { Scanner } from "../scanner"
import { FunctionCall } from "../ast/node"
import { EvaluationResult, EvaluationError, isDuration, EnvironmentValue, isEvaluationResult, isEvaluationFunction, isScalar } from "./types"
import { Environment } from "./environment";


/**
 * `Evaluator` implements the evaluation of a number of expressions.
 * It uses a `Scanner` to create a stream of tokens which are consumed
 * by a `Parser` which outputs an AST. This AST is fed into an 
 * `EvaluationVisitor` to actualy do the evaluation.
 */
export class Evaluator {
    constructor(private environment: Environment, private durationParser: DurationParser = new ComponentDurationParser()) { }

    /**
     * Evaluates the given expression.
     * @param expression the expression
     * @returns the evaluation result
     * @throws `ParseError` in case of parsing errors
     * @throws `EvaluationError` in case of evaluation errors
     */
    evaluate(expression: string): EvaluationResult {
        const ast = new Parser(new Scanner(expression), this.durationParser).expression()
        const visitor = new EvaluationVisitor(this.environment)
        ast.accept(visitor)
        return visitor.result
    }
}

/**
 * Convennience function to evaluate a single expression.
 * @param environment the environment
 * @param expression the expression
 * @returns the evaluation result
 * @throws `ParseError` in case of parsing errors
 * @throws `EvaluationError` in case of evaluation errors
 */
export function evaluate(environment: Environment, expression: string): EvaluationResult {
    return new Evaluator(environment).evaluate(expression)
}

class EvaluationVisitor implements AstNodeVisitor {
    private resultStack: Array<EvaluationResult> = []
    constructor(private readonly environment: Environment) { }

    get result(): EvaluationResult {
        return this.resultStack[0]
    }

    visitDuration(node: DurationNode): void {
        this.resultStack.push(node.duration)
    }

    visitScalar(node: ScalarNode): void {
        this.resultStack.push(node.value)
    }

    visitReference(node: ReferenceNode): void {
        const v = this.environment.get(node.name)
        if (!isEvaluationResult(v)) {
            throw new EvaluationError(`neither a number nor a duration: ${v}`)
        }
        this.resultStack.push(v)
    }

    visitBinaryOperator(node: BinaryOperatorNode): void {
        node.rightOperand.accept(this)
        node.leftOperand.accept(this)

        const left = this.resultStack.pop() as EvaluationResult
        const right = this.resultStack.pop() as EvaluationResult

        let result: EvaluationResult

        switch (node.operator) {
            case BinaryOperator.Add:
                result = this.add(left, right)
                break
            case BinaryOperator.Sub:
                result = this.sub(left, right)
                break
            case BinaryOperator.Times:
            default:
                result = this.times(left, right)
                break
        }

        this.resultStack.push(result)
    }

    visitFunctionCall(node: FunctionCall): void {
        const f = this.environment.get(node.functionName)
        if (!isEvaluationFunction(f)) {
            throw new EvaluationError(`not a function: ${f} with name "${node.functionName}"`)
        }

        const args: Array<EvaluationResult> = []
        node.args.forEach(a => {
            a.accept(this)
            args.push(this.resultStack.pop() as EvaluationResult)
        })

        this.resultStack.push(f(args))
    }

    private add(left: EvaluationResult, right: EvaluationResult): EvaluationResult {
        if (isDuration(left) && isDuration(right)) {
            return left.add(right)
        }

        if (isScalar(left) && isScalar(right)) {
            return left + right
        }

        throw new EvaluationError(`type mismatch: cannot add "${left}" and "${right}"`)
    }

    private sub(left: EvaluationResult, right: EvaluationResult): EvaluationResult {
        if (isDuration(left) && isDuration(right)) {
            return left.sub(right)
        }

        if (isScalar(left) && isScalar(right)) {
            return left - right
        }

        throw new EvaluationError(`type mismatch: cannot sub "${left}" and "${right}"`)
    }

    private times(left: EvaluationResult, right: EvaluationResult): EvaluationResult {
        if (isDuration(left) && isScalar(right)) {
            return left.times(right)
        }

        if (isScalar(left) && isDuration(right)) {
            return right.times(left)
        }

        if (isScalar(left) && isScalar(right)) {
            return left * right
        }

        throw new EvaluationError(`type mismatch: cannot multiply "${left}" and "${right}"`)
    }
}
