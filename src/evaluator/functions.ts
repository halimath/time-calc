//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Minute, Hour, Day, Week } from "../duration"
import { EvaluationResult, EvaluationError, isDuration, EvaluationFunction, isScalar } from "./types"

export const seconds = singleDuration((d: Duration) => {
    return d.seconds
})

export const fromSeconds = singleScalar((s: number) => {
    return new Duration(s)
})

export const minutes = singleDuration((d: Duration) => {
    return d.minutes
})

export const fromMinutes = singleScalar((s: number) => {
    return new Duration(s * Minute)
})

export const hours = singleDuration((d: Duration) => {
    return d.hours
})

export const fromHours = singleScalar((s: number) => {
    return new Duration(s * Hour)
})

export const days = singleDuration((d: Duration) => {
    return d.days
})

export const fromDays = singleScalar((s: number) => {
    return new Duration(s * Day)
})

export const weeks = singleDuration((d: Duration) => {
    return d.weeks
})

export const fromWeeks = singleScalar((s: number) => {
    return new Duration(s * Week)
})

function singleDuration<R extends EvaluationResult>(target: (arg: Duration) => R): EvaluationFunction {
    return function (args: Array<EvaluationResult>): EvaluationResult {
        if (args.length !== 1) {
            throw new EvaluationError(`${target.name}: expected exactly one argument but got ${args.length}`)
        }

        if (!isDuration(args[0])) {
            throw new EvaluationError(`${target.name}: expected a duration but got ${args[0]}`)
        }

        return target(args[0] as Duration)
    }
}

function singleScalar<R extends EvaluationResult>(target: (arg: number) => R): EvaluationFunction {
    return function (args: Array<EvaluationResult>): EvaluationResult {
        if (args.length !== 1) {
            throw new EvaluationError(`${target.name}: expected exactly one mumb but got ${args.length}`)
        }

        if (!isScalar(args[0])) {
            throw new EvaluationError(`${target.name}: expected a number but got ${args[0]}`)
        }

        return target(args[0] as number)
    }
}