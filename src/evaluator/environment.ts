//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { EvaluationError, EnvironmentValue } from "./types"
import { seconds, minutes, hours, days, weeks, fromSeconds, fromMinutes, fromHours, fromDays, fromWeeks } from "./functions"

/**
 * `Environment` implements a namespace for variables.
 */
export class Environment {
    static globalEnv(): Environment {
        const env = new Environment()
        env.set("Seconds", seconds)
        env.set("FromSeconds", fromSeconds)

        env.set("Minutes", minutes)
        env.set("FromMinutes", fromMinutes)

        env.set("Hours", hours)
        env.set("FromHours", fromHours)

        env.set("Days", days)
        env.set("FromDays", fromDays)

        env.set("Weeks", weeks)
        env.set("FromWeeks", fromWeeks)

        return env
    }

    private values: Map<string, EnvironmentValue> = new Map()

    set(name: string, value: EnvironmentValue): Environment {
        this.values.set(name, value)
        return this
    }

    contains(name: string): boolean {
        return this.values.has(name)
    }

    get(name: string): EnvironmentValue {
        if (!this.contains(name)) {
            throw new EvaluationError(`no such defined value: "${name}"`)
        }

        return this.values.get(name) as EnvironmentValue
    }
}
