//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Minute, Hour, Day, Week } from "./duration"

/**
 * `DurationFormatter` is the interface used for all components that provide formatting 
 * of `Duration` values.
 */
export interface DurationFormatter {
    /**
     * Formats the given `duration` value to a `string`.
     * @param d the `Duration`
     */
    format(d: Duration): string
}

/**
 * The `ComponentDurationFormatter` implements a `DurationFormatter` that formats
 * `Duration` values using the component notation.
 */
export class ComponentDurationFormatter implements DurationFormatter {
    format(dur: Duration): string {
        if (dur.seconds === 0) {
            return "0s"
        }

        const parts: Array<String> = []

        const s = dur.seconds % Minute
        let r = Math.floor(dur.seconds / Minute)
        if (s > 0) {
            parts.push(`${s}s`)
        }

        const m = r % (Hour / Minute)
        r = Math.floor(r / (Hour / Minute))
        if (m > 0) {
            parts.push(`${m}m`)
        }

        const h = r % (Day / Hour)
        r = Math.floor(r / (Day / Hour))
        if (h > 0) {
            parts.push(`${h}h`)
        }

        const d = r % (Week / Day)
        const w = Math.floor(r / (Week / Day))

        if (d > 0) {
            parts.push(`${d}d`)
        }

        if (w > 0) {
            parts.push(`${w}w`)
        }

        return parts.reverse().join(" ")
    }
}