//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

export const Second = 1
export const Minute = Second * 60
export const Hour = Minute * 60
export const Day = Hour * 8
export const Week = Day * 5

/**
 * `Duration` models a single duration value.
 */
export class Duration {
    constructor(public seconds: number = 0) { }

    get minutes(): number {
        return this.seconds / Minute
    }

    get hours(): number {
        return this.seconds / Hour
    }

    get days(): number {
        return this.seconds / Day
    }

    get weeks(): number {
        return this.seconds / Week
    }

    add(other: Duration): Duration {
        return new Duration(this.seconds + other.seconds)
    }

    sub(other: Duration): Duration {
        return new Duration(this.seconds - other.seconds)
    }

    times(scalar: number): Duration {
        return new Duration(this.seconds * scalar)
    }
}