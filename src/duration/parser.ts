//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Week, Day, Second, Minute, Hour } from "./duration"

/**
 * `ParseError` represents an error during parsing.
 */
export class DurationParseError {
    constructor(public message: string) { }

    toString(): string {
        return this.message
    }
}

/**
 * `DurationParser` describes the interface implemented by
 * parsers that parse a duration's string representation into
 * a `Duration` object.
 */
export interface DurationParser {

    /**
     * `parse` parses the given `expression` and returns the corresponding `Duration`
     * or throws a `ParseError`.
     * @param expression the expression to parse
     * @throws `ParseError` in case of an error
     */
    parse(expression: string): Duration
}

/**
 * `ComponentDurationParser` implements a `DurationParser`
 * for the time-calc component format.
 */
export class ComponentDurationParser implements DurationParser {
    parse(expression: string): Duration {
        let seconds = 0
        const normalized = expression.replace(/\s+/, "")
        const regex = /([0-9]+)([wdhms])/gi
        let match: RegExpExecArray | null
        while ((match = regex.exec(normalized)) !== null) {
            const n = parseInt(match[1])
            if (isNaN(n)) {
                throw new DurationParseError(`invalid duration expression "${expression}": invalid number: "${match[1]}"`)
            }

            switch (match[2]) {
                case "w":
                    seconds += n * Week
                    break
                case "d":
                    seconds += n * Day
                    break
                case "h":
                    seconds += n * Hour
                    break
                case "m":
                    seconds += n * Minute
                    break
                case "s":
                    seconds += n * Second
                    break

                default:
                    throw new DurationParseError(`invalid duration expression "${expression}": invalid unit code: "${match[2]}"`)
            }
        }
        return new Duration(seconds)
    }
}