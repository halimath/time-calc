//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { AstNodeVisitor, DurationNode, ScalarNode, ReferenceNode, BinaryOperatorNode, BinaryOperator } from "../ast"
import { DurationFormatter, ComponentDurationFormatter, DurationParser, ComponentDurationParser } from "../duration"
import { Parser } from "../parser"
import { Scanner } from "../scanner"
import { EvaluationResult, isScalar, isDuration } from "../evaluator"
import { FunctionCall } from "../ast/node";

/**
 * `formatResult` formats an `EvaluationResult`.
 * @param v the result to format
 * @param formatter the duration formatter to use - defaults to a `ComponentDurationFormatter`
 */
export function formatResult(v: EvaluationResult, formatter: DurationFormatter = new ComponentDurationFormatter()): string {
    if (isScalar(v)) {
        return v.toString()
    }

    if (isDuration(v)) {
        return formatter.format(v)
    }

    throw new Error(`unexpected result: ${v}`)
}

/**
 * `formatExpression` is a convenience function to format the given expression.
 * @param expression the expression to format
 * @param durationFormatter the duration formatter - defaults to a `ComponentDurationFormatter`
 * @param durationParser the duration parser - defaults to a `ComponentDurationParser`
 */
export function formatExpression(expression: string, durationFormatter: DurationFormatter = new ComponentDurationFormatter(), durationParser: DurationParser = new ComponentDurationParser()) {
    const ast = new Parser(new Scanner(expression), durationParser).expression()
    const formatter = new SourceFormatter(durationFormatter)
    ast.accept(formatter)
    return formatter.formattedSource
}

/**
 * `SourceFormatter` implements an `AstNodeVisitor` that does source formatting.
 */
export class SourceFormatter implements AstNodeVisitor {
    private sourceBuilder = new SourceBuilder()

    constructor(private durationFormatter: DurationFormatter) { }

    get formattedSource(): string {
        return this.sourceBuilder.toString()
    }

    visitDuration(node: DurationNode): void {
        this.sourceBuilder
            .space()
            .write(this.durationFormatter.format(node.duration))
    }

    visitScalar(node: ScalarNode): void {
        this.sourceBuilder
            .space()
            .write(node.value)
    }

    visitReference(node: ReferenceNode): void {
        this.sourceBuilder
            .space()
            .write(node.name)
    }

    visitBinaryOperator(node: BinaryOperatorNode): void {
        this.sourceBuilder
            .space()
            .write("(")

        node.leftOperand.accept(this)

        let operator: string = ""

        switch (node.operator) {
            case BinaryOperator.Add:
                operator = "+"
                break
            case BinaryOperator.Sub:
                operator = "-"
                break
            case BinaryOperator.Times:
                operator = "*"
                break
        }

        this.sourceBuilder
            .space()
            .write(operator)

        node.rightOperand.accept(this)

        this.sourceBuilder
            .write(")")
    }

    visitFunctionCall(node: FunctionCall): void {
        this.sourceBuilder
            .space()
            .write(node.functionName)
            .write("(")

        node.args.forEach((arg, idx) => {
            if (idx > 0) {
                this.sourceBuilder
                    .write(",")
            }
            arg.accept(this)
        })

        this.sourceBuilder
            .write(")")
    }
}

class SourceBuilder {
    private source: string = ""

    write(s: string): SourceBuilder
    write(n: number): SourceBuilder

    write(a: any): SourceBuilder {
        this.source += a
        return this
    }

    space(): SourceBuilder {
        if (this.source.length > 0 && this.source[this.source.length - 1] !== "(") {
            this.source += " "
        }

        return this
    }

    toString() {
        return "" + this.source
    }
}