//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Location, Token, TokenType } from "../token"

/**
 * SyntaxError models an error during scanning.
 */
export class SyntaxError {
    constructor(public message: string, public location: Location) { }

    toString(): string {
        return `${this.message} @ ${this.location}`
    }
}

/**
 * Scanner implements a token scanner for the time-calc language.
 */
export class Scanner {
    private position = 0
    private pushBackStack: Array<Token> = []

    constructor(private source: string) { }

    pushBack(t: Token): void {
        this.pushBackStack.push(t)
    }

    /**
     * Next scans the input and returns the next token.
     * next returns `null` when the end of input has been reached.
     * next throws a `SyntaxError` in case of an error.
     */
    next(): Token | null {
        if (this.pushBackStack.length > 0) {
            return this.pushBackStack.pop() as Token
        }

        this.skipWhitespace()
        if (this.position >= this.source.length) {
            return null
        }

        let t: Token | null

        t = this.punctuation()
        if (t !== null) {
            return t
        }

        t = this.parenthesis()
        if (t !== null) {
            return t
        }

        t = this.operator()
        if (t !== null) {
            return t
        }

        t = this.reference()
        if (t !== null) {
            return t
        }

        t = this.scalar()
        if (t !== null) {
            return t
        }

        t = this.duration()
        if (t !== null) {
            return t
        }

        throw new SyntaxError(`unexpected input: "${this.source.substr(this.position)}" @ ${this.position}`, new Location(this.position))
    }

    private reference(): Token | null {
        const start = this.position
        let end = start
        while (end < this.source.length) {
            if (!REFERENCE_CHAR_PATTERN.test(this.source.substr(end, 1))) {
                break
            }
            end++
        }

        if (start === end) {
            return null
        }

        this.position = end

        return new Token(new Location(start, end - start), TokenType.Identifier, this.source.substring(start, end))
    }

    private scalar(): Token | null {
        const start = this.position
        let end = start
        while (end < this.source.length) {
            if (SCALAR_CHAR_PATTERN.test(this.source.substr(end, 1))) {
                end++
                continue
            }

            if (DURATION_CHAR_PATTERN.test(this.source.substr(end, 1))) {
                return null
            }

            break
        }

        if (start === end) {
            return null
        }

        this.position = end

        return new Token(new Location(start, end - start), TokenType.ScalarLiteral, this.source.substring(start, end))
    }

    private operator(): Token | null {
        const char = this.source.substr(this.position, 1)

        if (char === "+") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.BinaryAdd, "+")
        }

        if (char === "-") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.BinarySub, "-")
        }

        if (char === "*") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.BinaryTimes, "*")
        }

        return null
    }

    private parenthesis(): Token | null {
        const char = this.source.substr(this.position, 1)

        if (char === "(") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.ParenLeft, "(")
        }

        if (char === ")") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.ParenRight, ")")
        }

        return null
    }

    private punctuation(): Token | null {
        const char = this.source.substr(this.position, 1)

        if (char === ",") {
            this.position++
            return new Token(new Location(this.position - 1, 1), TokenType.Comma, ",")
        }

        return null
    }

    private duration(): Token | null {
        const start = this.position
        let end = start
        while (end < this.source.length) {
            const char = this.source.substr(end, 1)
            if (char !== " " && !DURATION_CHAR_PATTERN.test(char)) {
                break
            }
            end++
        }

        if (start === end) {
            return null
        }

        this.position = end

        return new Token(new Location(start, end - start), TokenType.DurationLiteral, this.source.substring(start, end))
    }

    private skipWhitespace() {
        while (this.position < this.source.length) {
            const char = this.source.substr(this.position, 1)
            if (WHITESPACE.indexOf(char) >= 0) {
                this.position++
            } else {
                break
            }
        }
    }
}

const WHITESPACE = [" ", "\t", "\r", "\n"]
const REFERENCE_CHAR_PATTERN = /^[a-z_]$/i
const SCALAR_CHAR_PATTERN = /^[0-9.]$/
const DURATION_CHAR_PATTERN = /^[0-9wdhms]$/i