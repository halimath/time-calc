//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Minute, Hour, Day, Week, Second } from "../../src/duration"
import { EvaluationResult, Evaluator, Environment } from "../../src"

describe("Evaluator", () => {
    const cases: Array<[string, EvaluationResult]> = [
        [
            "1s",
            new Duration(1)
        ],
        [
            "17",
            17.0
        ],
        [
            "1m + 13s",
            new Duration(1 * Minute + 13 * Second)
        ],
        [
            "2 + 3 * 4",
            14.0
        ],
        [
            "(2 + 3) * 4",
            20.0
        ],
        [
            "Seconds((2m + 3s) * 3)",
            6 * Minute + 9 * Second
        ],
    ]

    cases.forEach(c => {
        const [expression, expected] = c
        test(`evaluate("${expression}")`, () => {
            const env = Environment.globalEnv()
            const evaluator = new Evaluator(env)
            expect(evaluator.evaluate(expression)).toEqual(expected)
        })
    })
})