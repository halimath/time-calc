//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Second, Minute, Hour, Day } from "../../src/duration"

describe("Duration", () => {
    test("minutes()", () => expect(new Duration(90 * Second).minutes).toEqual(1.5))
    test("hours()", () => expect(new Duration(90 * Minute).hours).toEqual(1.5))
    test("days()", () => expect(new Duration(12 * Hour).days).toEqual(1.5))
    test("weeks()", () => expect(new Duration(7 * Day + 4 * Hour).weeks).toEqual(1.5))

    test("add()", () => expect(new Duration(60).add(new Duration(30))).toEqual(new Duration(90)))
    test("sub()", () => expect(new Duration(60).sub(new Duration(30))).toEqual(new Duration(30)))
    test("times()", () => expect(new Duration(60).times(2.5)).toEqual(new Duration(150)))
})