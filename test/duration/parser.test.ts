//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Minute, Hour, Day, Week, Second } from "../../src/duration"
import { ComponentDurationParser } from "../../src/duration/parser"

describe("ComponentDurationParser", () => {
    const cases: Array<[string, Duration]> = [
        ["1s", new Duration(1)],
        ["1m", new Duration(1 * Minute)],
        ["1h", new Duration(1 * Hour)],
        ["1d", new Duration(1 * Day)],
        ["1w", new Duration(1 * Week)],
        ["1w 2d 3h 4m 5s", new Duration(1 * Week + 2 * Day + 3 * Hour + 4 * Minute + 5 * Second)],
    ]

    const p = new ComponentDurationParser()

    cases.forEach(c => {
        const [expression, expected] = c
        test(`parse("${expression}")`, () => {
            expect(p.parse(expression)).toEqual(expected)
        })
    })
})