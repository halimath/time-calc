//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Duration, Second, Minute, Hour, Day, ComponentDurationFormatter, Week } from "../../src/duration"

describe("ComponentDurationFormatter", () => {
    const f = new ComponentDurationFormatter()

    const cases: Array<[Duration, string]> = [
        [new Duration(0), "0s"],
        [new Duration(14), "14s"],
        [new Duration(61), "1m 1s"],
        [new Duration(Hour), "1h"],
        [new Duration(Week + 2 * Day + 3 * Hour + 4 * Minute + 5 * Second), "1w 2d 3h 4m 5s"],
    ]

    cases.forEach(c => test(`format(${c[0]}) === ${c[1]}`, () => expect(f.format(c[0])).toEqual(c[1])))
})