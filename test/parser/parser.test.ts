//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { AstNode, ScalarNode, BinaryOperatorNode, BinaryOperator, DurationNode } from "../../src/ast"
import { Location } from "../../src/token"
import { Parser } from "../../src/parser"
import { Scanner } from "../../src/scanner"
import { ComponentDurationParser, Duration, Minute, Hour } from "../../src/duration"
import { FunctionCall } from "../../src/ast/node";

describe("Parser", () => {
    const cases: Array<[string, AstNode]> = [
        [
            "15",
            new ScalarNode(new Location(0, 2), 15.0)
        ],
        [
            "2 + 3",
            new BinaryOperatorNode(new Location(2, 1), BinaryOperator.Add, new ScalarNode(new Location(0, 1), 2.0), new ScalarNode(new Location(4, 1), 3.0))
        ],
        [
            "2*3+4",
            new BinaryOperatorNode(new Location(3, 1), BinaryOperator.Add,
                new BinaryOperatorNode(new Location(1, 1), BinaryOperator.Times, new ScalarNode(new Location(0, 1), 2.0), new ScalarNode(new Location(2, 1), 3.0)),
                new ScalarNode(new Location(4, 1), 4.0)
            )
        ],
        [
            "2*(3+4)",
            new BinaryOperatorNode(new Location(1, 1), BinaryOperator.Times,
                new ScalarNode(new Location(0, 1), 2.0),
                new BinaryOperatorNode(new Location(4, 1), BinaryOperator.Add,
                    new ScalarNode(new Location(3, 1), 3.0),
                    new ScalarNode(new Location(5, 1), 4.0),
                )
            )
        ],
        [
            "2h 10m*3+4s",
            new BinaryOperatorNode(new Location(8, 1), BinaryOperator.Add,
                new BinaryOperatorNode(new Location(6, 1), BinaryOperator.Times, new DurationNode(new Location(0, 6), new Duration(2 * Hour + 10 * Minute)), new ScalarNode(new Location(7, 1), 3.0)),
                new DurationNode(new Location(9, 2), new Duration(4))
            )
        ],
        [
            "Seconds(2h 10m*3+4s)",
            new FunctionCall(new Location(0, 7), "Seconds", [
                new BinaryOperatorNode(new Location(16, 1), BinaryOperator.Add,
                    new BinaryOperatorNode(new Location(14, 1), BinaryOperator.Times, new DurationNode(new Location(8, 6), new Duration(2 * Hour + 10 * Minute)), new ScalarNode(new Location(15, 1), 3.0)),
                    new DurationNode(new Location(17, 2), new Duration(4))
                )
            ])
        ],
    ]

    cases.forEach(c => {
        const [source, expected] = c
        test(`expression: "${source}"`, () => {
            const actual = new Parser(new Scanner(source), new ComponentDurationParser()).expression()
            expect(actual).toEqual(expected)
        })
    })
})