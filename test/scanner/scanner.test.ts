//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { Scanner, SyntaxError } from "../../src/scanner"
import { Token, TokenType, Location } from "../../src/token"

describe("Scanner", () => {
    describe("next", () => {
        describe("with correct input", () => {
            const testCases: Array<[string, Array<Token>]> = [
                [
                    "", []],
                [
                    "    \t\n  \t", []],
                [
                    "foo", [new Token(new Location(0, 3), TokenType.Identifier, "foo"),],],
                [
                    "17", [new Token(new Location(0, 2), TokenType.ScalarLiteral, "17"),],],
                [
                    "17.3", [new Token(new Location(0, 4), TokenType.ScalarLiteral, "17.3"),],],
                [
                    "1w1d3h4m5s", [new Token(new Location(0, 10), TokenType.DurationLiteral, "1w1d3h4m5s"),],],
                [
                    "1w 1d 3h 4m 5s ", [new Token(new Location(0, 15), TokenType.DurationLiteral, "1w 1d 3h 4m 5s "),],],
                [
                    "+", [new Token(new Location(0, 1), TokenType.BinaryAdd, "+"),],],
                [
                    "-", [new Token(new Location(0, 1), TokenType.BinarySub, "-"),],],
                [
                    "*", [new Token(new Location(0, 1), TokenType.BinaryTimes, "*"),],],
                [
                    "(", [new Token(new Location(0, 1), TokenType.ParenLeft, "("),],],
                [
                    ")", [new Token(new Location(0, 1), TokenType.ParenRight, ")"),],],
                [
                    "2h 10m * 5.3 + 13s - 1w",
                    [
                        new Token(new Location(0, 7), TokenType.DurationLiteral, "2h 10m "),
                        new Token(new Location(7, 1), TokenType.BinaryTimes, "*"),
                        new Token(new Location(9, 3), TokenType.ScalarLiteral, "5.3"),
                        new Token(new Location(13, 1), TokenType.BinaryAdd, "+"),
                        new Token(new Location(15, 4), TokenType.DurationLiteral, "13s "),
                        new Token(new Location(19, 1), TokenType.BinarySub, "-"),
                        new Token(new Location(21, 2), TokenType.DurationLiteral, "1w"),
                    ]
                ],
                [
                    "(2h 10m * 5.3)",
                    [
                        new Token(new Location(0, 1), TokenType.ParenLeft, "("),
                        new Token(new Location(1, 7), TokenType.DurationLiteral, "2h 10m "),
                        new Token(new Location(8, 1), TokenType.BinaryTimes, "*"),
                        new Token(new Location(10, 3), TokenType.ScalarLiteral, "5.3"),
                        new Token(new Location(13, 1), TokenType.ParenRight, ")"),
                    ]
                ],
                [
                    ",",
                    [
                        new Token(new Location(0, 1), TokenType.Comma, ","),
                    ]
                ],
            ]

            testCases.forEach(testCase => {
                test(`Scanner.next("${testCase[0]}")`, () => {
                    const scanner = new Scanner(testCase[0])
                    const tokens: Array<Token> = []
                    let token: Token | null
                    while ((token = scanner.next()) !== null) {
                        tokens.push(token)
                    }

                    expect(tokens.length).toBe(testCase[1].length)

                    testCase[1].forEach((expected, i) => expect(tokens[i]).toEqual(expected))
                })
            })
        })

        describe("with incorrect input", () => {
            const testCases: Array<string> = [
                ":",
                "foo$",
            ]

            testCases.forEach(testCase => {
                test(`Scanner.next("${testCase}")`, () => {
                    const tokens: Array<Token> = []
                    let ok = false
                    try {
                        const scanner = new Scanner(testCase)

                        let token: Token | null
                        while ((token = scanner.next()) !== null) {
                            tokens.push(token)
                        }
                    } catch (e) {
                        if (!(e instanceof SyntaxError)) {
                            throw e
                        }
                        ok = true
                    } finally {
                        expect(ok).toBe(true)
                    }
                })
            })
        })
    })
})

