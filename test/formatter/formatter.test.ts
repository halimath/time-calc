//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { EvaluationResult, Duration, Minute, Second, formatResult, formatExpression } from "../../src"

describe("formatResult", () => {
    const cases: Array<[EvaluationResult, string]> = [
        [
            new Duration(2 * Minute + 3 * Second),
            "2m 3s"
        ],
        [
            2.3,
            "2.3"
        ],
    ]

    cases.forEach(c => {
        const [input, expected] = c
        test(`formatResult(${input})`, () => {
            expect(formatResult(input)).toBe(expected)
        })
    })

})

describe("formatExpression", () => {
    const cases: Array<[string, string]> = [
        [
            "2m3s",
            "2m 3s"
        ],
        [
            "2.3",
            "2.3"
        ],
        [
            "(2h - 13s) * (5-3)",
            "((2h - 13s) * (5 - 3))"
        ],
        [
            "Fn  (   2h,   30s )",
            "Fn(2h, 30s)",
        ],
    ]

    cases.forEach(c => {
        const [input, expected] = c
        test(`formatExpression(${input})`, () => {
            expect(formatExpression(input)).toBe(expected)
        })
    })

})