#!/usr/bin/env node
//
//  This file is part of time-calc.
//
//  Copyright (c) 2019 Alexander Metzner.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import { createInterface } from "readline"

import * as yargs from "yargs"

import { version } from "../package.json"

import * as timecalc from "../src"

function evaluateSingleExpression(env: timecalc.Environment, expression: string) {
    try {
        console.log(timecalc.formatResult(timecalc.evaluate(env, expression), new timecalc.ComponentDurationFormatter()))
    } catch (e) {
        console.log("Error during evaluation: " + e)
        process.exit(9)
    }
}

function format(expression: string) {
    console.log(timecalc.formatExpression(expression))
}

function repl(env: timecalc.Environment) {
    const evaluator = new timecalc.Evaluator(env)
    const formatter = new timecalc.ComponentDurationFormatter()
    console.log("Type expressions and press <RETURN> to evaluate.")
    console.log("Type CTRL+D or CTRL+C to quit.")

    const rl = createInterface({
        input: process.stdin,
        output: process.stdout
    })

    rl.on("close", () => {
        process.stdout.write("\n")
        process.exit(0)
    })

    rl.on("line", input => {
        try {
            const result = evaluator.evaluate(input)
            env.set("ans", result)
            const formatted = timecalc.formatResult(result, formatter)
            console.log(formatted)
            rl.setPrompt(`ans=${formatted}> `)
        } catch (e) {
            console.log("Error during evaluation: " + e)
        } finally {
            rl.prompt()
        }
    })

    rl.setPrompt("> ")

    rl.prompt()
}

const _ = yargs
    .scriptName("time-calc")
    .usage("$0 <cmd> [args]")

    .command("version", "Show time-calc's version", builder => {
        return builder
    }, () => {
        console.log(`time-calc v${version}`)
    })

    .command("eval <expression>", "Evaluate as single expression", builder => {
        return builder
            .positional("expression", {
                type: "string",
                default: "",
                describe: "the expression to evaluate"
            })
    }, argv => {
        const env = timecalc.Environment.globalEnv()
        evaluateSingleExpression(env, argv.expression)
    })

    .command("repl", "Start in read-evaluate-print-loop", builder => {
        return builder
    }, argv => {
        const env = timecalc.Environment.globalEnv()
        repl(env)
    })

    .command("format <expression>", "Format a given source expression", builder => {
        return builder
            .positional("expression", {
                type: "string",
                default: "",
                describe: "the expression to format"
            })
    }, argv => {
        format(argv.expression)
    })

    .command("$0", "", builder => builder, argv => {
        console.log("Missing command")
        process.exit(1)
    })
    .help()
    .argv